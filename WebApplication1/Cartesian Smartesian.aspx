﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cartesian Smartesian.aspx.cs" Inherits="WebApplication1.Cartesian_Smartesian" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cartesian Smartesian</title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Cartesian Smartesian</h1>
        <h2>Let's Play!</h2>
        <div>
            <asp:Label Text="Enter x-value" runat="server" />
            <asp:TextBox runat="server" ID="xValue"  />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="xValue" ErrorMessage="Please enter a number"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="chekNumberX" runat="server" 
            ErrorMessage="Invalid input.Please enter a number." ControlToValidate="xValue" 
            ValidationExpression="^[\-]?[0-9]*$"></asp:RegularExpressionValidator>
        </div>
        <br />
        <div>
            <asp:Label Text="Enter y-value" runat="server" />
            <asp:TextBox runat="server" ID="yValue"  />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="yValue" ErrorMessage="Please enter a number"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="chekNumberY" runat="server" 
            ErrorMessage="Invalid input.Please enter a number." ControlToValidate="yValue" 
            ValidationExpression="^[\-]?[0-9]*$"></asp:RegularExpressionValidator>
        </div>
        <br />
        <asp:Button Text="Click here to know the answer" ID="myButton" runat="server" OnClick="myButton_Click" />
    </form>
    <div id="answer" runat="server"></div>
</body>
</html>
