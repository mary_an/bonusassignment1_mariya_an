﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class String_Bling : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void palindromeButton_Click(object sender, EventArgs e)
        {
            string inputWord = word1.Text.Replace(" ", string.Empty).ToLower();
            
            bool flag = false;
            for (int i=0; i<inputWord.Length/2; i++)
            {
                if (inputWord[i] != inputWord[inputWord.Length - i - 1])
                {
                    flag = true;
                    break;
                }

            }
            if (!flag)
            {
                wordCheck.InnerHtml = "This is a palindrome!";
            }
            else
            {
                wordCheck.InnerHtml = "Ooops!This is not a palindrome.";
            }

        }
    }
}