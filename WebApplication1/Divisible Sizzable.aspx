﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Divisible Sizzable.aspx.cs" Inherits="WebApplication1.Divisible_Sizzable" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Divisible Sizzable</title>
</head>
<body>
    <form id="form1" runat="server">
        <h1> Divisible Sizzable</h1>
        <h2>Let's play!</h2>
        <div>
            <asp:Label Text="Enter a Number:" runat="server" />
            <asp:TextBox runat="server" ID="num1" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="num1" ErrorMessage="Please enter a number"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
            ErrorMessage="Invalid input.Please enter a number." ControlToValidate="num1" 
            ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
        </div>
       
        <br />
        <asp:Button Text="Click here to know if your Number is prime" runat="server" ID="primeNumberBtn" OnClick="primeNumberBtn_Click" />
    </form>
    <div id="primeAnswer" runat="server"></div>
</body>
</html>
