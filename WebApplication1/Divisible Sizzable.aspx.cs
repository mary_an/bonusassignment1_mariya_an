﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Divisible_Sizzable : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void primeNumberBtn_Click(object sender, EventArgs e)
        {
            int inputNumber = int.Parse(num1.Text);
            bool flag = false;
            for (int i=2; i<=inputNumber/2; i++)
            {
                if(inputNumber % i == 0)
                {
                    flag = true;
                    break;
                }
            }
            if (!flag)
            {
                primeAnswer.InnerHtml = inputNumber + " is a prime number.";
            }
            else
            {
                primeAnswer.InnerHtml = inputNumber + " is not a prime number.";
            }
        }
    }
}