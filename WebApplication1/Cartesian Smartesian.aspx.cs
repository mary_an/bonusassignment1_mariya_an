﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Cartesian_Smartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void myButton_Click(object sender, EventArgs e)
        {
            int xValueInput = int.Parse(xValue.Text);
            int yValueInput = int.Parse(yValue.Text);
            if (xValueInput==0 || yValueInput == 0)
            {
                answer.InnerHtml = "Please enter any namber, except for 0";
            }

           else if (xValueInput>0 && yValueInput > 0)
            {
                answer.InnerHtml = "The point falls into Quadrant 1";
            }
            else if(xValueInput>0 && yValueInput < 0)
            {
                answer.InnerHtml = "The point falls into Quadrant 4";
            }
            else if (xValueInput < 0 && yValueInput < 0)
            {
                answer.InnerHtml = "The point falls into Quadrant 3";
            }
            else
            {
                answer.InnerHtml = "The point falls into Quadrant 2";
            }

        }
    }
}