﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="String Bling.aspx.cs" Inherits="WebApplication1.String_Bling" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>String Bling</title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>String Bling</h1>
        <h2>Let's Play!</h2>
        <div>
            <asp:Label Text="Enter a Word:" runat="server" />
            <asp:TextBox runat="server" ID="word1" />
            <asp:RequiredFieldValidator ErrorMessage="Oh, come on...Let's play!Enter a word." ControlToValidate="word1" runat="server" />
            <asp:RegularExpressionValidator ErrorMessage="It's not a word!" ControlToValidate="word1" runat="server"  ValidationExpression="^[' 'a-zA-Z]+$"/>
        </div>
        <br />
        <h3>Do you want to know if your word is a palindrome?</h3>
        <asp:Button Text="Let's check!" runat="server" ID="palindromeButton" OnClick="palindromeButton_Click"/>
    </form>
    <div id="wordCheck" runat="server"></div>
</body>
</html>
